<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\application\controller
{
	require_once('types.hh');
	
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\helper\ObjectHelper;
	use nuclio\plugin\
	{
		database\datasource\manager\Manager as DataSourceManager,
		database\datasource\source\Source,
		http\request\Request,
		http\uri\URI,
		config\ConfigCollection,
		application\application\ApplicationException
	};
	use \ReflectionClass;
	
	<<
		__ConsistentConstruct,
		singleton
	>>
	abstract class Controller extends Plugin
	{
		protected Request $request;
		protected URI $URI;
		public ConfigCollection $config;
		
		public static function getInstance(/* HH_FIXME[4033] */ ...$args):this
		{
			$instance=ClassManager::getClassInstance(static::class,...$args);
			return ($instance instanceof static)?$instance:new static(...$args);
		}

		public function __construct(/* HH_FIXME[4033] */ ...$args)
		{
			parent::__construct();
			$args=new Vector($args);
			
			$URI=$args->get(0);
			if (!$URI instanceof URI)
			{
				throw new ApplicationException('Controller must accept a URI binding.');
			}
			
			$config=$args->get(1);
			if (!$config instanceof Map)
			{
				throw new ApplicationException('Controller must accept a config map.');
			}
			
			$this->URI		=$URI;
			$this->config	=$config;
			$this->request	=Request::getInstance();
			$reflection		=new ReflectionClass($this);
			$traits			=$reflection->getTraitNames();
			
			for ($i=0,$j=count($traits); $i<$j; $i++)
			{
				$methodName='init'.ObjectHelper::getBaseClassName($traits[$i]);
				if (method_exists($this,$methodName))
				{
					call_user_func_array([$this,$methodName],$args);
				}
			}
		}
		
		public function getRequest():Request
		{
			return $this->request;
		}
		
		public function getConfig():ConfigCollection
		{
			return $this->config;
		}
		
		public function getURI():URI
		{
			return $this->URI;
		}

		public function redirect(string $path):void
		{
			header('location:'.$path);
			exit();
		}
	}
}
