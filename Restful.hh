<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\application\controller
{
	use nuclio\plugin\application\responder\Responder;

	trait Restful
	{
		require extends Controller;
		
		private Map<int,string> $codes=Map
		{
			100=>'Continue',
			101=>'Switching Protocols',
			200=>'OK',
			201=>'Created',
			202=>'Accepted',
			203=>'Non-Authoritative Information',
			204=>'No Content',
			205=>'Reset Content',
			206=>'Partial Content',
			300=>'Multiple Choices',
			301=>'Moved Permanently',
			302=>'Found',
			303=>'See Other',
			304=>'Not Modified',
			305=>'Use Proxy',
			306=>'(Unused)',
			307=>'Temporary Redirect',
			400=>'Bad Request',
			401=>'Unauthorized',
			402=>'Payment Required',
			403=>'Forbidden',
			404=>'Not Found',
			405=>'Method Not Allowed',
			406=>'Not Acceptable',
			407=>'Proxy Authentication Required',
			408=>'Request Timeout',
			409=>'Conflict',
			410=>'Gone',
			411=>'Length Required',
			412=>'Precondition Failed',
			413=>'Request Entity Too Large',
			414=>'Request-URI Too Long',
			415=>'Unsupported Media Type',
			416=>'Requested Range Not Satisfiable',
			417=>'Expectation Failed',
			500=>'Internal Server Error',
			501=>'Not Implemented',
			502=>'Bad Gateway',
			503=>'Service Unavailable',
			504=>'Gateway Timeout',
			505=>'HTTP Version Not Supported'
		};
		private Vector<mixed> $responseCode;
		private string $format;
		
		public function initRestful():void
		{
			$this->responseCode = Vector{};
			if (!strpos($this->URI->getLast(),'.'))
			{
				$this->format='html';
			}
			else
			{
				$this->format=$this->URI->getLast()
				|>explode('.',$$)
				|>end($$);
			}
		}
		
		public function setResponseCode(int $code):void
		{
			if (!is_null($code) && !is_null($this->codes->get($code)))
			{
				$this->responseCode=Vector{$code,$this->codes->get($code)};
			}
		}
		
		public function respond(bool $success=true, string $message='', mixed $data=null):void
		{
			header('HTTP/1.1 '.(string)$this->responseCode->get(0).' '.(string)$this->responseCode->get(1));
			header("Access-Control-Allow-Origin: *");
			
			if($this->format=='html')
			{
				$x		=$this->responseCode->get(0);
				$y		=$this->responseCode->get(1);
				$HTML	='';
				if(is_string($data))
				{
					if(is_object($data)){
						$data = json_decode(json_encode($data),true);
					}
					$HTML=$data;
				}
				else if (!is_null($data))
				{
					$HTML=implode('<br>',$data);
				}
				$isSuccess=$success?'true':'false';
				$data=<<<HTML
<h1>{$x} {$y}</h1>
<h3>Success: {$isSuccess}</h3>
<h3>Message: {$message}</h3>
<hr>
<h3>Data</h3>
<h3>{$HTML}</h3>
HTML;

				$responder	=Responder::getInstance($this->format);
				$responder	->setData($data)
							->send();

				exit();
			}
			$responder=Responder::getInstance($this->format);
			$responder->setData
			(
				Map
				{
					'success'	=>$success,
					'message'	=>$message,
					'data'		=>$data
				}
			)
			->send();

			exit();
		}
	}
}
