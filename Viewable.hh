<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\application\controller
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\format\driver\template\twig\Twig;
	use \Twig_SimpleFunction;

	trait Viewable
	{
		require extends Controller;
		
		private Map<string,mixed> $keyVal;
		private Twig $twig;
		
		public function initViewable():void
		{
			$this->keyVal	=Map{};
			$config			=$this->config->get('twig');
			if (is_array($config))
			{
				$config=new Map($config);
			}
			else if (!$config instanceof Map)
			{
				$config=Map{};
			}
			$twigConfig		=new Map($config);
			$this->twig		=Twig::getInstance($twigConfig);
		}
		
		/**
		 * Render the given template.
		 * @param  string $template 	File name of the template.
		 */
		public function render(string $template):string
		{
			// $tempEngine=pathinfo($template,PATHINFO_FILENAME);
			// $tempEngine=explode('.',$tempEngine);
			// $tempEngine=end($tempEngine);
			
			$tempEngine=pathinfo($template,PATHINFO_FILENAME) |> explode('.',$$) |> end($$);
			
			$keyVals=$this->getKeyVals();
			if($tempEngine=='twig')
			{
				if(is_null($keyVals))
				{
					$compiled=$this->twig->render($template);
				}
				else
				{
					$compiled=$this->twig->render($template, $keyVals);
				}
				return $compiled;
			}
			else
			{
				throw new ControllerException("Only support twig for now!");
			}
		}
		
		public function set(string $key, mixed $val):this
		{
			$this->setKeyVal($key,$val);
			return $this;
		}
		
		public function setMany(Map<string,mixed> $keyVals):this
		{
			return $this->setKeyVals($keyVals);
		}
		
		public function get(string $key):mixed
		{
			return $this->keyVal->get($key);
		}

		/**
		 * Return all the keys vals specified.
		 * @return Map<string,mixed> All the keys and values.
		 */
		public function getKeyVals():Map<string,mixed>
		{
			return $this->keyVal;
		}

		/**
		 * Set single key with a given value.
		 * @param string $key Assign value to this key,
		 * @param mixed $val Assign this value.
		 */
		public function setKeyVal(string $key, mixed $val):this
		{
			$this->keyVal->set($key,$val);
			return $this;
		}

		/**
		 * Set multiple keys.
		 * @param ?Map<string,mixed> $keyVals Map object containing all the key and values.
		 */
		public function setKeyVals(Map<string,mixed> $keyVals):this
		{
			foreach($keyVals as $key=>$val)
			{
				$this->setKeyVal($key,$val);
			}
			return $this;
		}

		/**
		 * Set other path instead of view folder to render the template
		 * @param string      $path      New path to pu the template.
		 * @param string|null $namespace namespace of new path, @new_path
		 */
		public function addPath(string $path, ?string $namespace=null):bool
		{
			return $this->twig->addPath($path, $namespace);
		}
		
		public function generateUUID():string
		{
			return sprintf
			(
				'%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				// 32 bits for "time_low"
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
				
				// 16 bits for "time_mid"
				mt_rand( 0, 0xffff ),
				
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand( 0, 0x0fff ) | 0x4000,
				
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand( 0, 0x3fff ) | 0x8000,
				
				// 48 bits for "node"
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
			);
		}

		public function registerFunction(string $name,(function(...):mixed) $func):this
		{
			$function = new Twig_SimpleFunction($name,$func);
			$this->twig->addFunction($function);
			return $this;
		}
	}
}
