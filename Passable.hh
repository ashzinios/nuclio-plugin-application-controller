<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\application\controller
{
	use nuclio\core\ClassManager;
	use nuclio\helper\ObjectHelper;
	use nuclio\plugin\application\application\ApplicationException;

	trait Passable
	{
		require extends Controller;
		
		public function passControlTo(string $toController,string $action='run',Map<string,mixed> $data=Map{}):string
		{
			$controllerClass=get_class($this);
			
			$newController=ClassManager::getClassInstance
			(
				$toController,
				$this->getURI(),
				$this->getConfig()
			);
			if (!ObjectHelper::uses($newController,Viewable::class))
			{
				throw new ApplicationException(sprintf('passControlTo must be used to pass to a Controller which uses the "Viewable" trait. "%s" is not a View Controller.',$toController));
			}
			return call_user_func_array([$newController,$action],[$data]);
		}
	}
}
