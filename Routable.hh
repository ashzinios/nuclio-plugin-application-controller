<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\application\controller
{
	use nuclio\core\ClassManager;
	use nuclio\helper\ObjectHelper;
	use nuclio\plugin\application\application\ApplicationException;

	trait Routable
	{
		require extends Controller;
		
		public function routeControlTo(string $toController,string $action='run',Map<string,mixed> $data=Map{},bool $canOutput=false):Map<string,mixed>
		{
			$newController=ClassManager::getClassInstance
			(
				$toController,
				$this->getURI(),
				$this->getConfig()
			);
			if (!ObjectHelper::uses($newController,Viewable::class))
			{
				$result=call_user_func_array([$newController,$action],[$canOutput,$data]);
			}
			else
			{
				$result=call_user_func_array([$newController,$action],[$data]);
			}
			if ($result instanceof Map)
			{
				return $result;
			}
			else
			{
				return Map{'view'=>$result};
			}
		}
	}
}
